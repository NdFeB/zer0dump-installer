#!/bin/bash

# NOTE: zer0dump does NOT authenticate against the target AD, but only does password hash dump
# You can then use the NTLM hash to bypass auth with some Pass The Hash trick
# (example: psexec metasploit module, paste xxxxx:xxxxx hash as SMBPass option)

###################### GLOBALS #######################

_INSTALL_DIR="$HOME/CVE-2020-1472_ZeroLogon-zer0dump"

Red='\033[0;31m'
Blue='\033[0;34m'
Green='\033[0;32m'
Yellow='\033[0;33m'
Color_Off='\033[0m'

err=()

###################### FUNCTIONS #######################

function _printf () {
    if [ $# -eq 0 ]; then
        printf "\n"
        return
    fi
    local color=''
    local prefix=''

    case $1 in
        'error')   color="$Red";    prefix='[-]'; shift;;
        'info')    color="$Blue";   prefix='[*]'; shift;;
        'warning') color="$Yellow"; prefix='[!]'; shift;;
        'success') color="$Green";  prefix='[+]'; shift;;
    esac

    printf "%b%s %b%b" "$color" "$prefix" "$*" "$Color_Off"
}

# Call this with $1="exit" if the script should exit on error
function _check_error () {
    if [ ${#err[@]} -eq 0 ]; then
        return
    else
        for i in "${err[@]}"; do
            _printf error "$i"
        done
        [ "$1" == "exit" ] && exit 1
    fi
}

function _cleanup () {
    [ $venv_sourced == true ] && deactivate
}

trap _cleanup EXIT TERM INT

###################### VARIABLES DEFINITION #######################

venv_sourced=false

# REPOSITORIES
impacket_repo='https://github.com/SecureAuthCorp/impacket.git'
CrackMapExec_repo='https://github.com/byt3bl33d3r/CrackMapExec.git'
zer0dump_repo='https://github.com/bb00/zer0dump.git'

# VERSIONS AND CLONE PATHS

# ABOUT impacket: this is not the official requirement, but the official kept throwing
# errors at zer0dump runtime. Works well with this one.

impacket_vers='impacket-0.9.22.dev1+20200915.115225.78e8c8e4'
impacket_commit_hash='78e8c8e41b3f163f1271a01ce3f2bf3bb880f687'

impacket_git_dir="$_INSTALL_DIR"/deps/"$impacket_vers"/.git
impacket_work_tree="$_INSTALL_DIR"/deps/"$impacket_vers"

CrackMapExec_vers='crackmapexec-5.1.0.dev0'
CrackMapExec_commit_hash='14d12fba1e66665013ec6140346f8e66d43bc913'

CrackMapExec_git_dir="$_INSTALL_DIR"/deps/"$CrackMapExec_vers"/.git
CrackMapExec_work_tree="$_INSTALL_DIR"/deps/"$CrackMapExec_vers"

zer0dump_git_dir="$_INSTALL_DIR"/zer0dump/.git
zer0dump_work_tree="$_INSTALL_DIR"/zer0dump
zer0dump_commit_hash="423f695b0ba88aa73e9ff7fcd462ac6f2846f107"

###################### WORK TREE CREATION #######################

_printf info "Creating work tree...\n"
# Create install dir if it does not exist
[ ! -d "$_INSTALL_DIR" ] && mkdir -p "$_INSTALL_DIR"

# If it still does not exist or is non-writable, exit
if   [ ! -d "$_INSTALL_DIR" ]
then
    err+=("Error: could not create _INSTALL_DIR '$_INSTALL_DIR'\n")

elif [ ! -w "$_INSTALL_DIR" ]
then
    err+=("Error: _INSTALL_DIR '$_INSTALL_DIR' is not writable\n")

# If it is writable, then clean content
else
    for i in "$_INSTALL_DIR"/*; do
        rm -rf "$i"
        [ $? -ne 0 ] && err+=("Error while cleaning _INSTALL_DIR\n") && break
    done
fi

# Exit if any error occured
_check_error "exit"

# Create dependencies directory
mkdir "$_INSTALL_DIR"/deps

_printf success "Work tree is ready\n"

###################### VENV CREATION #######################

_printf info    "Creating python3 venv...\n"

# Create venv and source it
/usr/bin/env python3 -m venv "$_INSTALL_DIR"/venv

[ $? -ne 0 ] && err+=("Error while creating python3 venv\n")
_check_error "exit"

_printf success "Python3 venv ready\n"

###################### CLONING REPOSITORIES #######################

_printf info    "Cloning needed repositories...\n"

# Cloning repo
git clone --quiet --progress -n "$impacket_repo" "$impacket_work_tree"
[ $? -ne 0 ] && err+=("Error while cloning impacket repository\n")

git clone --quiet --progress -n "$CrackMapExec_repo" "$CrackMapExec_work_tree"
[ $? -ne 0 ] && err+=("Error while cloning CrackMapExec repository\n")

git clone --quiet --progress -n "$zer0dump_repo" "$zer0dump_work_tree"
[ $? -ne 0 ] && err+=("Error while cloning Zer0Dump repository\n")

_check_error "exit"

_printf info "Checking out correct versions...\n"

# Checking out wanted version
git --git-dir="$impacket_git_dir" \
    --work-tree="$impacket_work_tree" \
    checkout --quiet "$impacket_commit_hash"
[ $? -ne 0 ] && \
    err+=("Error while checking out impacket commit ${impacket_commit_hash:0:7}\n")

git --git-dir="$CrackMapExec_git_dir" \
    --work-tree="$CrackMapExec_work_tree" \
    checkout --quiet "$CrackMapExec_commit_hash"
[ $? -ne 0 ] && \
    err+=("Error while checking out CrackMapExec commit ${CrackMapExec_commit_hash:0:7}\n")

git --git-dir="$zer0dump_git_dir" \
    --work-tree="$zer0dump_work_tree" \
    checkout --quiet "$zer0dump_commit_hash"
[ $? -ne 0 ] && \
    err+=("Error while checking out Zer0Dump commit ${zer0dump_commit_hash:0:7}\n")

_check_error "exit"

_printf success "impacket, CrackMapExec and Zer0Dump checkouts ready\n"

sed -i -e 's/^impacket/#impacket/'          "$zer0dump_work_tree"/requirements.txt
sed -i -e 's/^crackmapexec/#crackmapexec/'  "$zer0dump_work_tree"/requirements.txt

###################### INSTALLING DEPENDENCIES #######################

_printf info    "Installing dependencies in the venv... This may take some time...\n"

source "$_INSTALL_DIR"/venv/bin/activate
venv_sourced=true

_pip3="$_INSTALL_DIR"/venv/bin/pip3

# To avoid errors when pip3 build wheels
_printf info    "Installing wheel...\n"
$_pip3 install -q wheel

[ $? -ne 0 ] && err+=("Error while installing wheel\n")

# Install impacket
_printf info    "Installing impacket...\n"
$_pip3 install -q "$impacket_work_tree"

[ $? -ne 0 ] && err+=("Error while installing impacket\n")

# Install CrackMapExec v5.1.0.dev
_printf info    "Installing CrackMapExec...\n"
$_pip3 install -q "$CrackMapExec_work_tree"

[ $? -ne 0 ] && err+=("Error while installing CrackMapExec\n")

# Install requirements for zer0dump
_printf info    "Installing Zer0Dump requirements...\n"
$_pip3 install -q -r "$zer0dump_work_tree"/requirements.txt

[ $? -ne 0 ] && err+=("Error while installing Zer0Dump requirements\n")

_check_error "exit"

_printf success "All done! You can now use Zer0Dump:\n\n"

printf "source $_INSTALL_DIR/venv/bin/activate\n"
printf "python3 $zer0dump_work_tree/zer0dump.py -h\n\n"

python3 $zer0dump_work_tree/zer0dump.py -h

exit $?

