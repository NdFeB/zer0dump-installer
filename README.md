# Zer0Dump Installer

## **/!\\ DO NOT USE IN A PRODUCTION ENVIRONMENT** /!\\

Install script for [Zer0Dump project](https://github.com/bb00/zer0dump "Zer0Dump on GitHub") ([CVE-2020-1472 / ZeroLogon](https://www.secura.com/blog/zero-logon)), for quick install on any Linux host or Docker

[Original ZeroLogon testing script](https://github.com/SecuraBV/CVE-2020-1472) (by [Secura](https://www.secura.com/), [@SecuraBV](https://twitter.com/SecuraBV "SecuraBV on Twitter"))

See also (for DC restore password): [dirkjanm/CVE-2020-1472](https://github.com/dirkjanm/CVE-2020-1472)
